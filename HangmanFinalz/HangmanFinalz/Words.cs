﻿using System;
using System.Windows.Forms;

namespace HangmanFinalz
{
    public partial class Words : Form
    {
        public static string path;

        public Words()
        {
            InitializeComponent();
        }

        private void carClick(object sender, EventArgs e)
        {
            path = @"..\..\..\Categories\Car.txt";
            lblInfo.Text = "CATEGORY SELECTED: CAR";
        }

        private void mountainsClick(object sender, EventArgs e)
        {
            path = @"..\..\..\Categories\Mountain.txt";
            lblInfo.Text = "CATEGORY SELECTED: MOUNTAINS";
        }

        private void moviesClick(object sender, EventArgs e)
        {
            path = @"..\..\..\Categories\Movie.txt";
            lblInfo.Text = "CATEGORY SELECTED: MOVIES";
        }

        private void statesClick(object sender, EventArgs e)
        {
            path = @"..\..\..\Categories\States.txt";
            lblInfo.Text = "CATEGORY SELECTED: STATES";
        }

        private void riversClick(object sender, EventArgs e)
        {
            path = @"..\..\..\Categories\River.txt";
            lblInfo.Text = "CATEGORY SELECTED: RIVERS";
        }

        private void allClick(object sender, EventArgs e)
        {
            path = @"..\..\..\Categories\All.txt";
            lblInfo.Text = "CATEGORY SELECTED: ALL";
        }

        private void okClick(object sender, EventArgs e)
        {
            if (lblInfo.Text != null)
            {
                this.Close();
                this.DialogResult = DialogResult.OK;
            }
        }

    }
}
