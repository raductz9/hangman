﻿using System;
using System.IO;
using System.Windows.Forms;

namespace HangmanFinalz
{
    public partial class Hangman : Form
    {
        string file;
        string target;
        int aux = 0;
        char guess;
        int tries;
        bool ok = false;

        public Hangman()
        {
            InitializeComponent();
            txtGuessBox.Text = "";
            lblWord.Text = null;
            target = "";
            tries = 0;
            pictureBox.Image = HangmanFinalz.Properties.Resources.hangman0;
            btnGuess.Enabled = false;
            txtGuessBox.Enabled = false;
        }

        private void randomWordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            resetGame();

            Words words = new Words();
            if (words.ShowDialog() == DialogResult.OK)
            {
                file = Words.path;
            }
            StreamReader reader = new StreamReader(file);
            while(reader.ReadLine() != null)
            {
                aux++;
            }
            reader.Dispose();
            reader = new StreamReader(file);
            Random random = new Random();
            for (int i = 0; i < random.Next(1, aux-1); i++)
                target = reader.ReadLine();
            reader.Dispose();
            //MessageBox.Show(target);

            setting();

            btnGuess.Enabled = true;
            txtGuessBox.Enabled = true;
        }


        private void guessClick(object sender, EventArgs e)
        {

                if(lblUsed.Text.IndexOf(txtGuessBox.Text[0]) == -1)
                 {
                     guess = txtGuessBox.Text[0];
                     lblUsed.Text = lblUsed.Text + guess + " ";
                 }

                for (int i = 0; i < target.Length; i++)
                {
                    if (Char.ToUpper(target[i]) == Char.ToUpper(guess))
                    {
                        ok = true;
                        lblWord.Text = lblWord.Text.Remove(i, 1);
                        lblWord.Text = lblWord.Text.Insert(i, guess.ToString());
                    }
                }
                if (lblWord.Text.ToUpper() == target.ToUpper())
                {
                    wonGame();
                }
                if (!ok)
                {
                    tries++;
                    switch (tries)
                    {
                        case 0:
                            pictureBox.Image = HangmanFinalz.Properties.Resources.hangman0;
                            break;
                        case 1:
                            pictureBox.Image = HangmanFinalz.Properties.Resources.hangman1;
                            break;
                        case 2:
                            pictureBox.Image = HangmanFinalz.Properties.Resources.hangman2;
                            break;
                        case 3:
                            pictureBox.Image = HangmanFinalz.Properties.Resources.hangman3;
                            break;
                        case 4:
                            pictureBox.Image = HangmanFinalz.Properties.Resources.hangman4;
                            break;
                        case 5:
                            pictureBox.Image = HangmanFinalz.Properties.Resources.hangman5;
                            break;
                        case 6:
                            pictureBox.Image = HangmanFinalz.Properties.Resources.hangman6;
                            lostGame();
                            break;
                    }
                }
                ok = false;
                txtGuessBox.Focus();
                txtGuessBox.SelectAll();
            
        }


        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("- The Hangman game -\n\n---> The game was created by Cretu Radu \n---> MVP COURSE - Teacher: Delia Duca\n---> Universitatea Transilvania Brasov");
        }


        private void setting()
        {
            lblWord.Text = "";
            for (int i = 0; i < target.Length; i++)
                lblWord.Text = lblWord.Text.Insert(i, "*");
        }


        private void lostGame()
        {
            MessageBox.Show("You lost! Secret word was: " + target + ".");
            resetGame();
        }

        private void wonGame()
        {
            MessageBox.Show("You won!");
            resetGame();
        }

        private void resetGame()
        {
            txtGuessBox.Text = "";
            lblWord.Text = "";
            lblUsed.Text = "";
            target = "";
            tries = 0;
            pictureBox.Image = HangmanFinalz.Properties.Resources.hangman0;
            btnGuess.Enabled = false;
            txtGuessBox.Enabled = false;
        }

    }
}
