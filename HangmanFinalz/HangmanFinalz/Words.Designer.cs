﻿using System;

namespace HangmanFinalz
{
    partial class Words
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCar = new System.Windows.Forms.Button();
            this.btnMountains = new System.Windows.Forms.Button();
            this.btnMovies = new System.Windows.Forms.Button();
            this.btnRivers = new System.Windows.Forms.Button();
            this.btnStates = new System.Windows.Forms.Button();
            this.btnAll = new System.Windows.Forms.Button();
            this.lblInfo = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCar
            // 
            this.btnCar.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCar.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCar.ForeColor = System.Drawing.SystemColors.Desktop;
            this.btnCar.Location = new System.Drawing.Point(-7, -1);
            this.btnCar.Name = "btnCar";
            this.btnCar.Size = new System.Drawing.Size(200, 65);
            this.btnCar.TabIndex = 0;
            this.btnCar.Text = "CAR";
            this.btnCar.UseVisualStyleBackColor = false;
            this.btnCar.Click += new System.EventHandler(this.carClick);
            // 
            // btnMountains
            // 
            this.btnMountains.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnMountains.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMountains.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMountains.ForeColor = System.Drawing.Color.Black;
            this.btnMountains.Location = new System.Drawing.Point(190, -1);
            this.btnMountains.Name = "btnMountains";
            this.btnMountains.Size = new System.Drawing.Size(200, 65);
            this.btnMountains.TabIndex = 1;
            this.btnMountains.Text = "MOUNTAINS";
            this.btnMountains.UseVisualStyleBackColor = false;
            this.btnMountains.Click += new System.EventHandler(this.mountainsClick);
            // 
            // btnMovies
            // 
            this.btnMovies.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnMovies.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMovies.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMovies.ForeColor = System.Drawing.Color.Black;
            this.btnMovies.Location = new System.Drawing.Point(-7, 61);
            this.btnMovies.Name = "btnMovies";
            this.btnMovies.Size = new System.Drawing.Size(200, 65);
            this.btnMovies.TabIndex = 2;
            this.btnMovies.Text = "MOVIES";
            this.btnMovies.UseVisualStyleBackColor = false;
            this.btnMovies.Click += new System.EventHandler(this.moviesClick);
            // 
            // btnRivers
            // 
            this.btnRivers.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnRivers.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRivers.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRivers.ForeColor = System.Drawing.Color.Black;
            this.btnRivers.Location = new System.Drawing.Point(190, 61);
            this.btnRivers.Name = "btnRivers";
            this.btnRivers.Size = new System.Drawing.Size(200, 65);
            this.btnRivers.TabIndex = 3;
            this.btnRivers.Text = "RIVERS";
            this.btnRivers.UseVisualStyleBackColor = false;
            this.btnRivers.Click += new System.EventHandler(this.riversClick);
            // 
            // btnStates
            // 
            this.btnStates.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnStates.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStates.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStates.ForeColor = System.Drawing.Color.Black;
            this.btnStates.Location = new System.Drawing.Point(-7, 123);
            this.btnStates.Name = "btnStates";
            this.btnStates.Size = new System.Drawing.Size(200, 65);
            this.btnStates.TabIndex = 4;
            this.btnStates.Text = "STATES";
            this.btnStates.UseVisualStyleBackColor = false;
            this.btnStates.Click += new System.EventHandler(this.statesClick);
            // 
            // btnAll
            // 
            this.btnAll.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAll.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAll.ForeColor = System.Drawing.Color.Black;
            this.btnAll.Location = new System.Drawing.Point(190, 123);
            this.btnAll.Name = "btnAll";
            this.btnAll.Size = new System.Drawing.Size(200, 65);
            this.btnAll.TabIndex = 5;
            this.btnAll.Text = "ALL";
            this.btnAll.UseVisualStyleBackColor = false;
            this.btnAll.Click += new System.EventHandler(this.allClick);
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfo.ForeColor = System.Drawing.Color.Black;
            this.lblInfo.Location = new System.Drawing.Point(3, 191);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(237, 27);
            this.lblInfo.TabIndex = 6;
            this.lblInfo.Text = "CATEGORY SELECTED: ";
            // 
            // btnOK
            // 
            this.btnOK.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnOK.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOK.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.ForeColor = System.Drawing.Color.Black;
            this.btnOK.Location = new System.Drawing.Point(-7, 218);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(400, 45);
            this.btnOK.TabIndex = 8;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = false;
            this.btnOK.Click += new System.EventHandler(this.okClick);
            // 
            // Words
            // 
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(384, 261);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblInfo);
            this.Controls.Add(this.btnAll);
            this.Controls.Add(this.btnStates);
            this.Controls.Add(this.btnRivers);
            this.Controls.Add(this.btnMovies);
            this.Controls.Add(this.btnMountains);
            this.Controls.Add(this.btnCar);
            this.ForeColor = System.Drawing.Color.Black;
            this.Name = "Words";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCar;
        private System.Windows.Forms.Button btnMountains;
        private System.Windows.Forms.Button btnMovies;
        private System.Windows.Forms.Button btnRivers;
        private System.Windows.Forms.Button btnStates;
        private System.Windows.Forms.Button btnAll;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.Button btnOK;
    }
}